'use strict';

let Video = require('twilio-video');

let activeRoom;
let previewTracks;
let identity;
let roomName;

function attachTracks(tracks, container) {
  tracks.forEach((track) => container.appendChild(track.attach()));
}

function attachParticipantTracks(participant, container) {
  let tracks = Array.from(participant.tracks.values());
  attachTracks(tracks, container);
}

function detachTracks(tracks) {
  tracks.forEach((track) => track.detach().forEach((detachedElement) => detachedElement.remove()));
}

function detachParticipantTracks(participant) {
  let tracks = Array.from(participant.tracks.values());
  detachTracks(tracks);
}

window.addEventListener('beforeunload', leaveRoomIfJoined);

$.getJSON('/token', (data) => {
  identity = data.identity;
  document.getElementById('room-controls').style.display = 'block';

  // Bind button to join Room.
  document.getElementById('button-join').onclick = () => {
    roomName = document.getElementById('room-name').value;
    if (!roomName) {
      return alert('Please enter a room name.');
    }

    let connectOptions = {name: roomName};

    if (previewTracks) {
      connectOptions.tracks = previewTracks;
    }

    Video.connect(data.token, connectOptions).then(roomJoined, (error) => {
      alert('Could not connect to Twilio: ' + error.message);
    });
  };

  document.getElementById('button-leave').onclick = () => {
    activeRoom.disconnect();
    setTimeout(() => document.getElementById('button-preview').click(), 4000);
  };
});

function roomJoined(room) {
  window.room = activeRoom = room;

  document.getElementById('button-join').style.display = 'none';
  document.getElementById('button-leave').style.display = 'inline';

  let previewContainer = document.getElementById('local-media');
  if (!previewContainer.querySelector('video')) {
    attachParticipantTracks(room.localParticipant, previewContainer);
  }

  room.participants.forEach((participant) => {
    let previewContainer = document.getElementById('remote-media');
    attachParticipantTracks(participant, previewContainer);
  });

  room.on('participantConnected', () => {
    setTimeout(() => document.getElementById('button-leave').click(), 8000);
  });

  room.on('trackAdded', (track) => {
    let previewContainer = document.getElementById('remote-media');
    attachTracks([track], previewContainer);
  });

  room.on('trackRemoved', (track) => {
    detachTracks([track]);
  });

  // When a Participant leaves the Room, detach its Tracks.
  room.on('participantDisconnected', (participant) => {
    detachParticipantTracks(participant);
  });

  // Once the LocalParticipant leaves the room, detach the Tracks
  // of all Participants, including that of the LocalParticipant.
  room.on('disconnected', () => {
    if (previewTracks) {
      previewTracks.forEach((track) => {
        track.stop();
      });
    }
    detachParticipantTracks(room.localParticipant);
    room.participants.forEach(detachParticipantTracks);
    activeRoom = null;
    document.getElementById('button-join').style.display = 'inline';
    document.getElementById('button-leave').style.display = 'none';
    previewTracks = null;
    window.previewTracks = null;
  });
}

// Preview LocalParticipant's Tracks.
document.getElementById('button-preview').onclick = () => {
  let localTracksPromise = previewTracks ? Promise.resolve(previewTracks) : Video.createLocalTracks();

  localTracksPromise.then((tracks) => {
    window.previewTracks = previewTracks = tracks;
    let previewContainer = document.getElementById('local-media');
    if (!previewContainer.querySelector('video')) {
      attachTracks(tracks, previewContainer);
    }

    document.getElementById('room-name').value = 'test';
    setTimeout(() => document.getElementById('button-join').click(), 4000);
  }, (error) => {
    alert('Unable to access Camera and Microphone \n' + error);
  });
};

// Leave Room.
function leaveRoomIfJoined() {
  if (activeRoom) {
    activeRoom.disconnect();
  }
}
